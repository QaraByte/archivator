﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace archivator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string s = "";

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtPath.Text != String.Empty)
            {
                Zip(txtPath.Text, txtPath.Text + ".trotro");
                //lbArchived.Text = s;
                txtInfo.Text = "Файл архива: " + s;
            }
            else
                MessageBox.Show("Выберите файл для архивирования!");
        }

        public void Zip(string FileRes, string FileDest)
        {
            FileStream fs = null;   //исходный файл
            FileStream rs = null;   //архив
            long dlina = 0;

            try
            {
                if (File.Exists(FileDest))
                    File.Delete(FileDest);
                string Format = FileRes.Substring(FileRes.LastIndexOf('.') + 1);
                fs = new FileStream(FileRes, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                rs = new FileStream(FileDest, FileMode.CreateNew);

                //сначала сохраним формат исходного файла
                rs.WriteByte((byte)Format.Length);
                for (int i = 0; i < Format.Length; ++i)
                    rs.WriteByte((byte)Format[i]);

                List<byte> Bt = new List<byte>();
                List<byte> nBt = new List<byte>();
                while (fs.Position < fs.Length)
                {
                    byte B = (byte)fs.ReadByte();
                    if (Bt.Count == 0)
                        Bt.Add(B);
                    else if (Bt[Bt.Count - 1] != B)
                    {
                        //неповторяющиеся байты
                        Bt.Add(B);
                        if (Bt.Count == 255)
                        {
                            rs.WriteByte((byte)0);
                            rs.WriteByte((byte)255);
                            rs.Write(Bt.ToArray(), 0, 255);
                            Bt.Clear();
                        }
                    }
                    else
                    {
                        //повтор
                        if (Bt.Count != 1)
                        {
                            //в буфере могут быть неповторяющиеся байты
                            //их нужно сохранить
                            rs.WriteByte((byte)0);
                            rs.WriteByte((byte)(Bt.Count - 1));
                            rs.Write(Bt.ToArray(), 0, Bt.Count - 1);
                            Bt.RemoveRange(0, Bt.Count - 1);
                        }
                        Bt.Add(B);
                        while ((B = (byte)fs.ReadByte()) == Bt[0])
                        {
                            //пока идут повторы сохраняем их в буфер
                            Bt.Add(B);
                            if (Bt.Count == 255)
                            {
                                rs.WriteByte((byte)255);
                                rs.WriteByte(Bt[0]);
                                Bt.Clear();
                                break;
                            }
                        }
                        if (Bt.Count > 0)
                        {
                            //если в буфере что-то есть, сохраняем это
                            rs.WriteByte((byte)Bt.Count);
                            rs.WriteByte(Bt[0]);
                            Bt.Clear();
                            Bt.Add(B);
                        }
                    }
                    dlina = fs.Length;
                    
                }
                if (Bt.Count > 0)
                {
                    //после просмотра файла у нас может быть буфер с неповторяющимися байтами
                    rs.WriteByte((byte)0);
                    rs.WriteByte((byte)Bt.Count);
                    rs.Write(Bt.ToArray(), 0, Bt.Count);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (fs != null) fs.Close();
                if (rs != null) rs.Close();
            }
            s= fs.Name.ToString() + ".trotro";
            lbArchived.Text = dlina.ToString();
        }

        private void btn_uncompress_Click(object sender, EventArgs e)
        {
            OpenFileDialog trotro = new OpenFileDialog();
            trotro.Filter = "Archive files(*.trotro) | *.trotro";

            if (trotro.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    txtInfo.Text = trotro.FileName + "\r\n";
                    //txtInfo.Text += trotro.InitialDirectory;
                    //txtInfo.Text += trotro.CheckPathExists.ToString() + "\r\n";
                    //txtInfo.Text+= Path.GetDirectoryName(trotro.FileName);
                    UnZip(trotro.FileName, Path.GetDirectoryName(trotro.FileName));
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            if (openFile.ShowDialog()==DialogResult.OK)
                try
                {
                    txtPath.Text = openFile.FileName;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
        }

        public void UnZip(string FileRes, string FolderDest)
        {
            if (!File.Exists(FileRes)) return;
            FileStream fs = null;
            FileStream rs = null;
            try
            {
                if (FolderDest[FolderDest.Length - 1] != '\\') FolderDest += '\\';
                string FileName = FileRes.Split('\\')[FileRes.Split('\\').Length - 1];
                string Name = FolderDest + FileName.Substring(0, FileName.LastIndexOf('.'));
                string Format = ".";
                fs = new FileStream(FileRes, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                int FormatLen = fs.ReadByte();
                for (int i = 0; i < FormatLen; ++i)
                    Format += (char)fs.ReadByte();
                if (File.Exists(Name + Format))
                    File.Delete(Name + Format);
                rs = new FileStream(Name + Format, FileMode.CreateNew);
                while (fs.Position < fs.Length)
                {
                    int Bt = fs.ReadByte();
                    if (Bt == 0) //различные байты
                    {
                        Bt = fs.ReadByte();
                        for (int j = 0; j < Bt; ++j)
                        {
                            byte b = (byte)fs.ReadByte();
                            rs.WriteByte(b);
                        }
                    }
                    else //повторы
                    {
                        int Value = fs.ReadByte();
                        for (int j = 0; j < Bt; ++j)
                            rs.WriteByte((byte)Value);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (fs != null) fs.Close();
                if (rs != null) rs.Close();
            }
            txtInfo.Text = "Извлечено в " + FolderDest;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (txtPath.Text != "")
            {
                txtInfo.Text = txtPath.Text.LastIndexOf('.').ToString() + "\r\n";
                txtInfo.Text += txtPath.Text.Substring(txtPath.Text.LastIndexOf('.')) + Environment.NewLine;
                txtInfo.Text += txtPath.Text.Substring(txtPath.Text.LastIndexOf('.') + 1);
            }
        }

        private void btnWriteByte_Click(object sender, EventArgs e)
        {
            string str = "test002.data";
            byte[] dataArray = new byte[1000];

            new Random().NextBytes(dataArray);

            FileStream fs = new FileStream(str, FileMode.Create);

            for (int i=0; i<dataArray.Length; i++)                  // Write the data to the file, byte by byte.
            {
                fs.WriteByte(dataArray[i]);
                txtInfo.Text += dataArray[i] + "\r\n";
            }
            fs.Seek(0, SeekOrigin.Begin);                           // Set the stream position to the beginning of the file.

            for (int i=0; i<fs.Length; i++)                         // Read and verify the data.
            {
                if (dataArray[i]!=fs.ReadByte())
                {
                    txtInfo.Text += "Error writing data!";
                    return;
                }
            }

            txtInfo.Text += String.Format("The data was written to {0} and verified.", fs.Name);
        }
    }
}
